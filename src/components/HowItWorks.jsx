import React, { useRef } from "react";
import { chipImg, frameImg, frameVideo } from "../utils";
import { useGSAP } from "@gsap/react";
import gsap from "gsap";
import { animateWithGsap } from "../utils/animations";


const HowItWorks = () => {

    const videoRef = useRef();

    useGSAP(() => {
        gsap.from('#chip', {
            scrollTrigger: {
                trigger: '#chip',
                start: '20% bottom'
            },
            opacity: 0,
            scale: 2,
            duration: 2,
            ease: 'power2.inOut'
        });

        animateWithGsap('.g_fadeIn', {
            opacity: 1, y: 0, ease: 'power2.inOut'
        })
    }, [])

    return (
        <section className="common-padding">
            <div className="screen-max-width">
                <div id="chip" className="flex-center w-full my-20" >
                    <img src={chipImg} alt="chip" width={180} height={180} />
                </div>

                <div className="flex flex-col items-center">
                    <h2 className="hiw-title">
                        Lorem, ipsum dolor.
                        <br /> A monster win for gaming
                    </h2>
                    <p className="hew-subtitle">Lorem ipsum dolor sit amet consectetur adipisicing elit. Sint, ullam nesciunt accusantium blanditiis unde explicabo! Corporis ipsum laborum earum? Culpa?</p>
                </div>

                <div className="mt-10 md:mt-20 mb-14">
                    <div className="relative flex-center h-full">
                        <div className="overlow-hidden">
                            <img src={frameImg} alt="frame" className="bg-transparent relative z-10" />
                        </div>
                        <div className="hiw-video">
                            <video className="pointer-events-none" preload="none" autoPlay muted playsInline ref={videoRef} src={frameVideo} typeof="video/mp4"></video>
                        </div>
                    </div>

                    <p className="text-gray font-semibold text-center mt-3">Lorem, ipsum dolor.</p>
                    <div className="hiw-text-container">
                        <div className="flex flex-1 justify-center flex-col">
                            <p className="hiw-text g_fadeIn">
                                Lorem ipsum dolor sit amet. {' '}

                                <span className="text-white">
                                    Lorem ipsum dolor sit, amet consectetur adipisicing elit. Veniam a error eligendi! In, excepturi! Qui amet tenetur pariatur in officiis.                                </span>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit impedit voluptatibus quam aliquid veritatis. Animi ab maiores rem nihil sint iusto voluptatibus quia dolore ipsa nemo? Nihil, aut numquam. Sint?
                            </p>
                        </div>
                    </div>


                    <p className="feature-text g_text">
                        Lorem ipsum dolor sit amet consectetur, adipisicing elit. Sequi nisi officia quibusdam error inventore deleniti, animi aperiam dignissimos ullam! In! {' '}

                        <span className="text-white">
                            Lorem ipsum dolor sit amet consectetur, adipisicing elit. Error excepturi, optio explicabo porro voluptates incidunt.
                        </span>
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Explicabo libero dolores nobis nulla culpa non?
                    </p>

                    <div className="flex-1 flex justify-center flex-col g_fadeIn">
                        <p className="hiw-text">New</p>
                        <p className="hiw-bigtext">Lorem, ipsum.</p>
                        <p className="hiw-text">Lorem. </p>
                    </div>

                </div>
            </div>
        </section>
    )
}

export default HowItWorks;